package com.avs.saurabh.two_states_feedback;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    EditText name,phoneNumber,birthday,anniversary;
    Button submit;
    Calendar calendar;
    DatePickerDialog.OnDateSetListener date;
    public static int BIRTHDAY = 100;
    public static int ANNIVERSARY = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.editTextName);
        phoneNumber = findViewById(R.id.editTextPhone);
        birthday = findViewById(R.id.editTextBirthday);
        anniversary = findViewById(R.id.editTextAnniversary);
        submit = findViewById(R.id.buttonMain);
        calendar = Calendar.getInstance();

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                int tag = (Integer)view.getTag();
                if(tag == BIRTHDAY) {
                    updateLabel(birthday);
                }
                else if(tag == ANNIVERSARY) {
                    updateLabel(anniversary);
                }
            }

        };

        birthday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setTag(BIRTHDAY);
                datePickerDialog.show();
            }
        });

        anniversary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setTag(ANNIVERSARY);
                datePickerDialog.show();
            }
        });
        if(isNetworkConnected()) {
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String nameText = name.getText().toString();
                    String phoneText = phoneNumber.getText().toString();
                    String birthdayText = birthday.getText().toString();
                    String anniversaryText = anniversary.getText().toString();

                    if (nameText.isEmpty()) {
                        name.setError("Please Enter your Name");
                    } else if (phoneText.isEmpty() || phoneText.length() < 10) {
                        phoneNumber.setError("Please Enter your Phone Number");
                    } else if (birthdayText.isEmpty()) {
                        birthday.setError("Please Enter your Birthday");
                    }
                    else {
                        startActivity(new Intent(MainActivity.this, Ratings.class)
                                .putExtra("name", nameText)
                                .putExtra("phoneNumber", phoneText)
                                .putExtra("birthday", birthdayText)
                                .putExtra("anniversary", anniversaryText));
                    }
                }
            });
        }
        else {
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Info");
                alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setButton(Dialog.BUTTON_POSITIVE,"OK",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertDialog.show();
            } catch (Exception e) {
                Log.d("Show Dialog: ",  e.getMessage());
            }
        }
    }

    private void updateLabel(EditText editText) {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        editText.setText(sdf.format(calendar.getTime()));
    }

    @Override
    public void onBackPressed() {
        //do nothing..
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
