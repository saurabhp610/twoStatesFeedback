package com.avs.saurabh.two_states_feedback;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

public class Ratings extends AppCompatActivity {

    RatingBar ambience,food,service,valueForMoney;
    Button buttonRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratings);
        ambience = findViewById(R.id.ratingBarAmbience);
        food = findViewById(R.id.ratingBarFood);
        service = findViewById(R.id.ratingBarService);
        valueForMoney = findViewById(R.id.ratingBarSitting);
        buttonRating = findViewById(R.id.buttonRating);

        buttonRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Float ambienceRating = ambience.getRating();
                Float foodRating = food.getRating();
                Float serviceRating = service.getRating();
                Float valueForMoneyRating = valueForMoney.getRating();

                if(ambienceRating == 0 || foodRating == 0 || serviceRating == 0 || valueForMoneyRating == 0 ) {
                    Toast.makeText(Ratings.this, "Please rate each section !!!", Toast.LENGTH_LONG).show();
                }
                else {
                    Intent intent = getIntent();
                    startActivity(new Intent(Ratings.this,Suggestions.class)
                                    .putExtra("name",intent.getStringExtra("name"))
                                    .putExtra("phoneNumber", intent.getStringExtra("phoneNumber"))
                                    .putExtra("birthday", intent.getStringExtra("birthday"))
                                    .putExtra("anniversary", intent.getStringExtra("anniversary"))
                                    .putExtra("ambienceRating", ambienceRating)
                                    .putExtra("foodRating", foodRating)
                                    .putExtra("serviceRating", serviceRating)
                                    .putExtra("valueForMoneyRating", valueForMoneyRating)
                            );

                }
            }
        });
    }
}
