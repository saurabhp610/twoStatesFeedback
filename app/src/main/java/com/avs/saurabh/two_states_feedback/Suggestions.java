package com.avs.saurabh.two_states_feedback;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class Suggestions extends AppCompatActivity {

    public static String FEEDBACK_URL = "http://mondofinedine.com/twostates/feedback.php";
    EditText comments;
    Button submit;
    String nameText, phoneText, commentsText, birthdayText, anniversaryText;
    Float ambienceRating, foodRating, serviceRating, valueForMoneyRating, defaultRating = 1f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestions);
        Intent intent = getIntent();
        nameText = intent.getStringExtra("name");
        phoneText = intent.getStringExtra("phoneNumber");
        birthdayText = intent.getStringExtra("birthday");
        anniversaryText = intent.getStringExtra("anniversary");
        ambienceRating = intent.getFloatExtra("ambienceRating", defaultRating);
        foodRating = intent.getFloatExtra("foodRating", defaultRating);
        serviceRating = intent.getFloatExtra("serviceRating", defaultRating);
        valueForMoneyRating = intent.getFloatExtra("valueForMoneyRating", defaultRating);

        comments = findViewById(R.id.suggestions);
        submit = findViewById(R.id.buttonSuggestion);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commentsText = comments.getText().toString();

                String data = "Name: " + nameText + "\n Phone Number: " + phoneText +
                        "\n Birthday: " + birthdayText + "\n Anniversary: " + anniversaryText +
                        "\n Ambience: " + ambienceRating + "\n Food: " + foodRating +
                        "\n Service: " + serviceRating + "\n Sitting: " + valueForMoneyRating +
                        "\n Comments: " + commentsText;

                Log.d( "data sent:", data);
                postFeedBack(nameText, phoneText, birthdayText, anniversaryText, ambienceRating, foodRating,
                        serviceRating, valueForMoneyRating, commentsText);
                startActivity(new Intent(Suggestions.this, MainActivity.class));
            }
        });

    }

    private void postFeedBack(final String nameText, final String phoneText, final String birthdayText, final String anniversaryText,
                              final Float ambienceRating, final  Float foodRating, final Float serviceRating, final Float valueForMoneyRating,
                              final String commentsText){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, FEEDBACK_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response feedback",response);
                        Toast.makeText(Suggestions.this, "Successfully recorded feedback !!!", Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Suggestions.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<>();
                Log.d("name sent:",nameText);
                params.put("name",nameText);
                params.put("phone_no",phoneText);
                params.put("birthday",birthdayText);
                params.put("anniversary",anniversaryText);
                params.put("ambience_rating", String.valueOf(ambienceRating));
                params.put("food_rating", String.valueOf(foodRating));
                params.put("service_rating", String.valueOf(serviceRating));
                params.put("valueformoney_rating", String.valueOf(valueForMoneyRating));
                params.put("comments", String.valueOf(commentsText));
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
